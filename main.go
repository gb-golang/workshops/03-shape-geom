package main

type Shape struct {
	Name string
}

func (s Shape) GetName() string {
	return s.Name
}

func (s Shape) Area() float64 {
	return 0.0
}

func main() {
	// circle := Circle{Shape{"Круг"}, 5}
	// rectangle := Rectangle{Shape{"Прямоугольник"}, 4, 6}
	//
	// fmt.Printf("%s: Площадь = %.2f\n", circle.GetName(), circle.Area())
	// fmt.Printf("%s: Площадь = %.2f\n", rectangle.GetName(), rectangle.Area())
}
